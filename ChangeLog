2008-04-20  Étienne Bersac  <bersace03@gmail.com>

	* package/cifrado/cifrado.py: Permettre de sauvegarder le jeu si
	on a changé la police.
	(Cifrado.imprimer): Imprimer le bon numéro de message lorsqu'on un
	imprime un message seul.

	* package/cifrado/message.py (charger): Ne pas instancier des
	émission d'une force inexistante.

	* package/cifrado/cifrado.py (Cifrado.impr_message): Tronquer
	l'entête chef s'il dépasse.

2008-04-18  Étienne Bersac  <bersace03@gmail.com>

	* package/cifrado/cifrado.py: Utiliser le nom du jeu comme titre
	de la fenêtre.

	* package/cifrado/cifrado.py, package/cifrado/emission.py:
	Utilisation de la police dans la colonne 'Configuration' du
	chiffrement.

	* package/cifrado/emission.py (Emission.substituer): Substitution
	également dans l'en-tête chef.

	* package/cifrado/message.py (Message.forces_row_changed): Par
	défaut, ne pas envoyer un message existant à un force créée.

	* package/cifrado/codeurs/police.py (Codeur): Aucun interligne
	lorsqu'on change uniquement la police.

	* package/cifrado/message.py: Ne pas trier les variables.

	* package/cifrado/cifrado.py (Cifrado.on_msg_selected):
	sélectionner le premier destinaire effectif à la sélection d'un
	message.

	* package/cifrado/emission.py (Emission.convertir): Ne substituer
	que les variable explicitement définie.
	(Emission.createXML): Ne pas enregistrer les variables vides.

	* package/cifrado/cifrado.glade, package/cifrado/cifrado.py,
	package/cifrado/emission.py, package/cifrado/message.py: Ajout de
	la substitution de variable dans les message par émission.

	* package/cifrado/cifrado.py (Cifrado.impr_message): Déplace le
	numéro de message côté scout pour s'assuré qu'il n'est pas dans
	les marges d'impression.

2008-04-06  Étienne Bersac  <bersace03@gmail.com>

	* package/cifrado/codeurs/vigenere.py (Codeur.convertir): Idem
	pour le vigenere : On décale en arrière pour chiffrer et en avant
	pour déchiffrer.

	* package/cifrado/codeurs/roulement.py (Codeur.convertir): Inverse
	le roulement, de sorte que "A vaut K" signifie qu'un A doit être
	remplacé par un K dans le texte pour déchiffrer (et non pour
	chiffrer).

2008-04-04  Étienne Bersac  <bersace03@gmail.com>

	* package/cifrado/codeurs/templier.py: Ajout du chiffrage
	Templier.

	* package/cifrado/codeurs/braille.py: Ajout du chiffrage en
	braille.

	* package/cifrado/codeurs/samourai.py: Ajout du chiffrage
	samouraï.

	* package/cifrado/cifrado.py (Cifrado.on_msg_changed)
	(Cifrado.on_trame_changed): Enregistre à la volé la modification
	des champs multilignes plutôt qu'à la perte du focus.
	(Cifrado.impr_message): Listage du chiffrement dans le bandeau
	chef.
	(Cifrado.impr_message): Corrige l'impression du n° de message dans
	la partie chiffrée.

	* package/cifrado/codeurs/tictactoe.py (Codeur.convertir): Ajout
	d'un chiffreur tictactoe.

	* package/cifrado/codeurs/police.py (Codeur.sauver): Nouveau
	chiffreur pour sélectionner la police par message.

2008-04-03  Étienne Bersac  <bersace03@laposte.net>

	* data/cifrado.desktop.in, package/cifrado/cifrado.py: gèstion des
	url (file://, etc.)

	* cifrado.in: Ouvre le dernier grand-jeu par défaut.

	* data/cifrado.desktop.in (Exec): Déclaration du support d'un
	fichier en paramètre de la commande d'exécution.

	* package/cifrado/cifrado.py (Cifrado.sync): Désactive l'interface
	de sauvegarde si inutile.
	(Cifrado.nouveau): asceptisation du jeu par défaut.

2008-04-02  Étienne Bersac  <bersace03@laposte.net>

	* package/cifrado/cifrado.py (Cifrado.sauver): Utiliser
	xml.dom.minidom.toprettyxml() plutôt que xml.dom.ext.

2008-01-18  Étienne Bersac  <bersace03@laposte.net>

	* package/cifrado/cifrado.py (Cifrado): Gestion des document
	récents.

	* cifrado.in: Corrige "crash si pas d'argument".

	* data/Makefile.am, data/x-cifrado.xml: Ajout du type mime
	application/x-cifrado .

	* data/Makefile.am, data/cifrado.desktop.in: Enregistre l'icône de
	type et le type mime associé.

2008-01-17  Étienne Bersac  <bersace03@laposte.net>

	* package/cifrado/cifrado.py (Cifrado): Corrige des
	désynchronisation abusive du jeu en mémoire et du fichier.

	* cifrado.in: Ouvre un fichie passé en paramètre.

2007-11-14  Étienne Bersac <bersace03%40laposte.net>

	* package/cifrado/cifrado.py: 
	  Impression des forces et des lieux dans la feuille de chef.

2007-10-14  Étienne Bersac <bersace03%40laposte.net>

	* ChangeLog, configure.ac, manuel/C/cifrado.xml.in,
	  manuel/cifrado.omf.in, manuel/C/images,
	  manuel/C/images/chiffrement-props.png,
	  manuel/C/images/convertir.png, manuel/C/images/envoi-actif.png,
	  manuel/C/images/envoi-inactif.png, manuel/C/images/forces.png,
	  manuel/C/images/imprimer.png, manuel/C/images/jeu.png,
	  manuel/C/images/lieux.png, manuel/C/images/messages.png,
	  manuel/Makefile.am, package/cifrado/cifrado.glade,
	  package/cifrado/cifrado.py, package/cifrado/jeu.py, TODO: Ajout
	  du manuel, accesible depuis l'interface.

2007-10-14  Étienne Bersac <bersace03%40laposte.net>

	* ChangeLog, package/cifrado/codeurs/chiffrer.py,
	  package/cifrado/codeurs/roulement.py: Correction de bogues dans
	  le chargement de fichier. Révise la gestion des propriétés.

2007-10-14  Étienne Bersac <bersace03%40laposte.net>

	* ChangeLog, package/cifrado/cifrado.py: Désactive les boutons
	  imprimer/supprimer message si aucun message n'est sélectionné.

2007-10-14  Étienne Bersac <bersace03%40laposte.net>

	* ChangeLog, package/cifrado/cifrado.py: Ferme avant de quitter,
	  permet de demander la sauvegarde.

2007-10-13  Étienne Bersac  <bersace03@laposte.net>

	* Suppression de la gestion des règles de grand-jeu. Mieux vaut
	utiliser un message.

2007-10-12  Étienne Bersac <bersace03%40laposte.net>

	* ChangeLog, package/cifrado/cifrado.glade: Ajout des infobulles.

2007-10-12  Étienne Bersac <bersace03%40laposte.net>

	* ChangeLog, package/cifrado/cifrado.py: Imprime les messages dans
	  l'ordre des forces.

2007-10-12  Étienne Bersac <bersace03%40laposte.net>

	* ChangeLog, package/cifrado/cifrado.py: Numérote correctement les
	  messages.


version 0.4.1

	
2007-10-12  Étienne Bersac <bersace03%40laposte.net>

	* ChangeLog, package/cifrado/codeurs/morse.py: Ajout d'une option
	  pour le morse : inverser — et ·.

2007-10-12  Étienne Bersac <bersace03%40laposte.net>

	* ChangeLog, package/cifrado/codeurs/caesar.py,
	  package/cifrado/codeurs/__init__.py: Ajout du codage Cæsar.

2007-10-12  Étienne Bersac <bersace03%40laposte.net>

	* ChangeLog, package/cifrado/codeurs/__init__.py,
	  package/cifrado/codeurs/polybius.py: Ajout du codage "Polybius".

2007-10-12  Étienne Bersac <bersace03%40laposte.net>

	* ChangeLog, package/cifrado/codeurs/__init__.py,
	  package/cifrado/codeurs/inverser.py: Ajout du codage "Inverser".

2007-10-12  Étienne Bersac <bersace03%40laposte.net>

	* ChangeLog, data/cifrado.glade, package/cifrado/cifrado.py: Ajout
	  de la conversion à la volée (sans impression) avec copie dans le
	  presse-papier

2007-10-12  Étienne Bersac <bersace03%40laposte.net>

	* ChangeLog, data/cifrado.glade: Renommage de l'item "propriété"
	  en "mise-en-page".

2007-10-12  Étienne Bersac <bersace03%40laposte.net>

	* ChangeLog, data/cifrado.glade: Permission d'avoir 0 chefs (pour
	  n'imprimer aucune feuille supplémentaires).

