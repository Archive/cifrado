# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

import  gobject
import  gtk

import  force
import  lieu
import  message

COL_LIEU_NOM,   COL_LIEU                                = range(2)
COL_FORCE_NOM,  COL_FORCE                               = range(2)
COL_MSG_EXP,    COL_MSG_TITRE,  COL_MSG_DATE, COL_MSG   = range(4)

def charger(el):
    jeu = Jeu(nom = el.getAttribute('nom'),
              nb_chefs = el.getAttribute('nb-chefs'),
              trame = el.getElementsByTagName('trame')[0].childNodes[0].nodeValue.strip(),
              police = el.getAttribute('police'))

    for e in el.getElementsByTagName('lieu'):
        jeu.ajouter_lieu(lieu.charger(e))

    for e in el.getElementsByTagName('force'):
        jeu.ajouter_force(force.charger(e))

    for e in el.getElementsByTagName('message'):
        jeu.ajouter_message(message.charger(e, jeu))

    jeu.sync = True

    return jeu

class Jeu:
    def __init__(self, nom="",trame="", nb_chefs=4, police='sans normal 9'):
        self.nom        = nom
        self.trame      = trame
        self.nb_chefs   = nb_chefs
        self.sync       = True
        self.police     = police

        # lieux
        self.lieux      = gtk.ListStore(gobject.TYPE_STRING,
                                        gobject.TYPE_PYOBJECT)
        # forces
        # pixbuf ?
        self.forces    = gtk.ListStore(gobject.TYPE_STRING,
                                       gobject.TYPE_PYOBJECT)

        # messages
        self.messages   = gtk.ListStore(gobject.TYPE_STRING,
                                        gobject.TYPE_STRING,
                                        gobject.TYPE_STRING,
                                        gobject.TYPE_PYOBJECT)


    def ajouter_lieu(self, lieu):
        self.lieux.append([lieu.nom, lieu])

    def ajouter_force(self, force):
        self.forces.append([force.nom, force])

    def ajouter_message(self, msg):
        return self.messages.append([msg.exp.nom,
                                     msg.titre,
                                     msg.date,
                                     msg])


    def get_force_depuis_nom(self, nom):
        iter = self.forces.get_iter_first()
        while iter is not None and self.forces.iter_is_valid(iter):
            force, = self.forces.get(iter, COL_FORCE)
            if force.nom == nom:
                return force
            iter = self.forces.iter_next(iter)

    def get_lieu_depuis_nom(self, nom):
        iter = self.lieux.get_iter_first()
        while iter is not None and self.lieux.iter_is_valid(iter):
            lieu, = self.lieux.get(iter, COL_LIEU)
            if lieu.nom == nom:
                return lieu
            iter = self.lieux.iter_next(iter)



    def createXML(self, doc):
        j = doc.createElement('jeu')
        j.setAttribute('nom', self.nom)
        j.setAttribute('nb-chefs', str(self.nb_chefs))
        j.setAttribute('police', self.police);

        e = doc.createElement('trame');
        j.appendChild(e);
        e.appendChild(doc.createTextNode(self.trame))


        iter = self.forces.get_iter_first()
        while iter is not None and self.forces.iter_is_valid(iter):
            f = self.forces.get_value(iter, COL_FORCE)
            j.appendChild(f.createXML(doc))
            iter = self.forces.iter_next(iter)

        iter = self.lieux.get_iter_first()
        while iter is not None and self.lieux.iter_is_valid(iter):
            l = self.lieux.get_value(iter, COL_LIEU)
            j.appendChild(l.createXML(doc))
            iter = self.lieux.iter_next(iter)

        iter = self.messages.get_iter_first()
        while iter is not None and self.messages.iter_is_valid(iter):
            m = self.messages.get_value(iter, COL_MSG)
            j.appendChild(m.createXML(doc))
            iter = self.messages.iter_next(iter)

        return j
