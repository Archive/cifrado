# -*- coding: utf-8 -*-

# Cifrado - un modéliseur de messages de grand-jeu
# Copyright © 2006-2008 Étienne Bersac — Tous droits réservés.

import  time
import  os.path
import  gnome
import  gtk
import  gtk.glade               as glade
import  gobject
import  pango
import  xdg.BaseDirectory       as xdgbd
from    xml.dom.minidom import  Document,parse

from    .       import jeu
from    .       import message
from    .       import emission
from    .force  import Force
from    .lieu   import Lieu
from    .       import codeurs
from    .       import defs

import urllib
from    urllib  import urlopen

COL_COD_NOM,COL_COD_CLASSE = range(2)

# Cette classe gère :
# * Vision globale du jeu courant (creation, impression, sauvegarde)
# * l'interface
class Cifrado:
    """Classe principale de Cifrado, gérant l'interface graphique"""

    def __init__(self):

        self.program = gnome.init(defs.__package__,
                                  defs.__version__)

        gtk.window_set_default_icon_name('cifrado')
        self.glade = glade.XML(defs.__glade__)
        dic = {
            # quitter
            'on_cifrado_fenetre_delete_event':          self.on_cifrado_fenetre_delete_event,
            'on_quitter_btn_clicked':                   self.on_quitter_btn_clicked_event,
            'on_menu_jeu_quitter_activate':             self.on_menu_jeu_quitter_activate,
            'on_menu_jeu_fermer_activate':              self.on_menu_jeu_fermer_activate,
            # ouvrir
            'on_menu_jeu_nv_activate':                  self.on_menu_jeu_nv_activate,
            'on_menu_jeu_ouv_activate':                 self.on_menu_jeu_ouv_activate,
            # enregistrer
            'on_enregistrer_btn_clicked':               self.on_enregistrer_btn_clicked,
            'on_menu_jeu_enr_activate':                 self.on_menu_jeu_enr_activate,
            'on_menu_jeu_enr_sous_activate':            self.on_menu_jeu_enr_sous_activate,
            # aide
            'on_menu_aide_apropos_activate':            self.on_menu_aide_apropos_activate,
            'on_apropos_response':                      self.on_apropos_response,
            'on_menu_aide_aide_activate':               self.on_menu_aide_aide_activate,
            # jeu
            'on_jeu_nb_chefs_champ_value_changed':      self.on_jeu_nb_chefs_champ_value_changed,
            'on_jeu_nom_champ_changed':                 self.on_jeu_nom_champ_changed,
            # force
            'on_force_nv_btn_clicked':                  self.on_force_nv_btn_clicked,
            'on_force_nom_edited':                      self.on_force_nom_edited,
            'on_force_suppr_btn_clicked':               self.on_force_suppr_btn_clicked,
            # lieu
            'on_lieu_nv_btn_clicked':                   self.on_lieu_nv_btn_clicked,
            'on_lieu_nom_edited':                       self.on_lieu_nom_edited,
            'on_lieu_suppr_btn_clicked':                self.on_lieu_suppr_btn_clicked,
            # messages
            'on_msg_nv_btn_clicked':                    self.on_msg_nv_btn_clicked,
            'on_msg_impr_btn_clicked':                  self.on_msg_impr_btn_clicked,
            'on_msg_suppr_btn_clicked':                 self.on_msg_suppr_btn_clicked,
            # emissions
            'on_em_dest_select_changed':                self.on_em_dest_select_changed,
            'on_em_env_champ_toggled':                  self.on_em_env_champ_toggled,
            'on_em_lieu_select_changed':                self.on_em_lieu_select_changed,
            # codeurs
            'on_codeur_nv_btn_clicked':                 self.on_codeur_nv_btn_clicked,
            'on_codeur_prop_btn_clicked':               self.on_codeur_prop_btn_clicked,
            'on_codeur_conv_btn_clicked':               self.on_codeur_conv_btn_clicked,
            'on_codeur_suppr_btn_clicked':              self.on_codeur_suppr_btn_clicked,
            # imprimer
            'on_menu_jeu_page_prop_activate':           self.on_menu_jeu_page_prop_activate,
            'on_menu_jeu_imprimer_activate':            self.on_menu_jeu_imprimer_activate,
            'on_imprimer_btn_clicked':                  self.on_imprimer_btn_clicked,
            'on_msg_impr_btn_clicked':                  self.on_msg_impr_btn_clicked,
            # autre
            'on_trame_exp_activate':                    self.on_champ_exp_activate,
            # police
            'on_menu_police_choisir_activate':          self.on_menu_police_choisir_activate,
            'on_selecteur_police_dialog_response':      self.on_selecteur_police_dialog_response,
            }
        self.glade.signal_autoconnect(dic)
        self.glade.get_widget('msg_liste').get_selection().connect('changed', self.on_msg_selected)

        self.glade.get_widget('jeu_trame_champ').get_buffer().connect('changed', self.on_trame_changed)
        self.glade.get_widget('msg_champ').get_buffer().connect('changed', self.on_msg_changed)

        # FORCES
        liste = self.glade.get_widget('forces_liste')

        # nom
        renderer = gtk.CellRendererText()
        renderer.set_property('editable', True)
        renderer.connect('edited', self.on_force_nom_edited)
        colonne = gtk.TreeViewColumn('Nom', renderer,
                                     text=jeu.COL_FORCE_NOM)
        colonne.set_expand(True)
        liste.append_column(colonne)

        # emblème
#         renderer = gtk.CellRendererPixbuf()
#         renderer.connect('edited', self.on_force_nom_edited)
#         colonne = gtk.TreeViewColumn('Emblème', renderer,
#                                      text=jeu.COL_FORCE_NOM)
#         colonne.set_expand(True)
#         liste.append_column(colonne)

        # LIEUX
        liste = self.glade.get_widget('lieux_liste')

        renderer = gtk.CellRendererText()
        renderer.set_property('editable', True)
        renderer.connect('edited', self.on_lieu_nom_edited)
        colonne = gtk.TreeViewColumn('Nom', renderer,
                                     text=jeu.COL_LIEU_NOM)
        liste.append_column(colonne)

        # MESSAGES
        liste = self.glade.get_widget('msg_liste')

        # exp
        renderer = gtk.CellRendererCombo()
        renderer.set_property('editable', True)
        renderer.set_property('has-entry', False)
        renderer.set_property('text-column', jeu.COL_FORCE_NOM)
        self.msg_exp_renderer = renderer
        renderer.connect('edited', self.on_msg_exp_edited)
        colonne = gtk.TreeViewColumn('Expéditeur', renderer,
                                     text=jeu.COL_MSG_EXP)
        liste.append_column(colonne)

        # titre
        renderer = gtk.CellRendererText()
        renderer.set_property('editable', True)
        renderer.connect('edited', self.on_msg_titre_edited)
        colonne = gtk.TreeViewColumn('Titre', renderer,
                                     text=jeu.COL_MSG_TITRE)
        colonne.set_expand(True)
        liste.append_column(colonne)

        # date
        renderer = gtk.CellRendererText()
        renderer.set_property('editable', True)
        renderer.connect('edited', self.on_msg_date_edited)
        colonne = gtk.TreeViewColumn('Date', renderer,
                                     text=jeu.COL_MSG_DATE)
        colonne.set_expand(False)
        liste.append_column(colonne)

        # EMISSION

        combo = self.glade.get_widget('em_dest_select')
        renderer = gtk.CellRendererText()
        combo.pack_start(renderer, False)
        combo.set_attributes(renderer, text=jeu.COL_FORCE_NOM)

        combo = self.glade.get_widget('em_lieu_select')
        renderer = gtk.CellRendererText()
        combo.pack_start(renderer, False)
        combo.set_attributes(renderer, text=jeu.COL_LIEU_NOM)

        # CODEURS
        self.codeurs = gtk.ListStore(gobject.TYPE_STRING,
                                     gobject.TYPE_PYOBJECT)
        
        for m in codeurs.__all__:
            module = __import__('codeurs.'+m, globals(), locals(), ['codeurs.'+m], -1)
            module.iter = self.codeurs.append([module.Codeur.nom, module.Codeur])

        liste = self.glade.get_widget('codeur_liste')
        liste.get_selection().connect('changed', self.on_codeur_selection_changed)

        # codeur
        renderer = gtk.CellRendererCombo()
        renderer.set_property('editable', True)
        renderer.set_property('has-entry', False)
        renderer.set_property('text-column', COL_COD_NOM)
        renderer.set_property('model', self.codeurs)
        self.codeur_renderer = renderer
        renderer.connect('edited', self.on_codeur_edited)
        colonne = gtk.TreeViewColumn('Chiffrement', renderer,
                                     text=emission.COL_COD_NOM)
        colonne.set_expand(True)
        liste.append_column(colonne)

        # prop
        renderer = gtk.CellRendererText()
        colonne = gtk.TreeViewColumn('Configuration', renderer,
                                     text=emission.COL_COD_PROP,
                                     font=emission.COL_COD_POL)
        colonne.set_expand(True)
        liste.append_column(colonne)


        # Fenêtre de conversion.
        d = self.glade.get_widget('conversion_dialog')
        d.add_buttons(gtk.STOCK_COPY, gtk.RESPONSE_APPLY,
                      gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE)
        c = gtk.clipboard_get(gtk.gdk.SELECTION_PRIMARY)
        self.glade.get_widget('conversion_champ').get_buffer().add_selection_clipboard(c)

        

        # SUBSTITUTIONS
        self.substitutions = gtk.ListStore(gobject.TYPE_STRING,
                                           gobject.TYPE_STRING)
        
        liste = self.glade.get_widget('substitutions_liste')
        liste.set_model(self.substitutions)

        # Variable
        renderer = gtk.CellRendererText()
        colonne = gtk.TreeViewColumn('Variable', renderer,
                                     text=emission.COL_SUB_VAR)
        colonne.set_expand(True)
        liste.append_column(colonne)

        # Valeur
        renderer = gtk.CellRendererText()
        renderer.set_property('editable', True)
        renderer.connect('edited', self.on_substitution_edited)
        colonne = gtk.TreeViewColumn('Valeur', renderer,
                                     text=emission.COL_COD_PROP)
        colonne.set_expand(True)
        liste.append_column(colonne)


        # CHARGEMENT

        f = gtk.FileFilter()
        f.add_mime_type('application/x-cifrado+xml')
        f.set_name('Grand-jeu cifrado')

        d = self.glade.get_widget('select_fichier_ouv')
        d.add_buttons(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                      gtk.STOCK_OPEN, gtk.RESPONSE_OK)
        d.add_filter(f)


        # SAUVEGARDE

        d = self.glade.get_widget('select_fichier_enr')
        d.add_buttons(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                      gtk.STOCK_SAVE, gtk.RESPONSE_OK)
        d.add_filter(f)

        # IMPRESSION
        self.page_setup = gtk.PageSetup()
        # hack dégueux :
        self.page_setup.set_bottom_margin(self.page_setup.get_top_margin(gtk.UNIT_POINTS), gtk.UNIT_POINTS)
        self.print_settings = gtk.PrintSettings()

        # À PROPOS
        d = self.glade.get_widget('apropos')
        d.set_name(defs.__package__)
        d.set_version(defs.__version__)
        d.set_comments(defs.__description__)
#         d.set_website_label('Site web de Cifrado')
#         d.set_website(defs.__website__)

        self.jeu = None
        self.fichier = None

    def lancer(self):
        self.glade.get_widget('cifrado_fenetre').show_all()
        gtk.main()

    def imprimer(self,msg_cour):
        op = gtk.PrintOperation()
        op.set_print_settings(self.print_settings)
        op.set_default_page_setup(self.page_setup)
        op.connect('begin-print', self.impr_begin_print)
        op.connect('draw-page', self.impr_draw_page)
        self.print_settings.set("output-uri", self.jeu.nom+".pdf")

        self.pages = []
        ms = []
        n = 0
        no = 0
        if msg_cour is None:
            op.set_job_name("Grand-Jeu « %s »"%(self.jeu.nom))
            # ajouter nd_chefs pages de chef
            for i in range(self.jeu.nb_chefs):
                self.pages.append(self.jeu)
                n+=1

        else:
            op.set_job_name("Message « %s » du Grand-Jeu « %s »"%(msg_cour.titre,
                                                                  self.jeu.nom))

        i = self.jeu.messages.get_iter_first()
        # boucler sur les messages
        while i is not None and self.jeu.messages.iter_is_valid(i):
            msg = self.jeu.messages.get_value(i, jeu.COL_MSG)
            imprimer = msg_cour is None or msg_cour is msg

            if imprimer:
                ms.append(msg)

            # boucler les émission
            iter = self.jeu.forces.get_iter_first()
            while iter is not None and self.jeu.forces.iter_is_valid(iter):
                force = self.jeu.forces.get_value(iter, jeu.COL_FORCE)
                em = msg.emissions[force]
                # ajouter le coupler message/emission si envoi
                if em.env is True:
                    no+=1
                    if imprimer:
                        n+=1
                        self.pages.append([no,msg,em])
                iter = self.jeu.forces.iter_next(iter)
            i = self.jeu.messages.iter_next(i)


        # Si aucun messages à imprimer
        if not n > 0:
            d = gtk.MessageDialog(self.glade.get_widget('cifrado_fenetre'),
                                  gtk.DIALOG_MODAL,
                                  gtk.MESSAGE_WARNING,
                                  gtk.BUTTONS_OK,
                                  None)
            d.set_markup("<big><b>Aucun messages à imprimer !!</b></big>")
            d.format_secondary_text("Créez de nouveaux messages et sélectionnez "
                                    "leurs destinataires à qui envoyer")
            d.run()
            d.destroy()
            return

        op.set_n_pages(n)
        op.run(gtk.PRINT_OPERATION_ACTION_PRINT_DIALOG,
               self.glade.get_widget('cifrado_fenetre'))

    def quitter(self):
        self.fermer()
        gtk.main_quit()
        

    def charger(self, fichier):
        if fichier is None:
            d = self.glade.get_widget('select_fichier_ouv')
            d.set_current_folder(xdgbd._home)
            r = d.run()
            d.hide()
            if r != gtk.RESPONSE_OK.__index__():
                return
            else:
                fichier = d.get_filename()

        self.fermer()
        self.fichier = fichier
        flux = urlopen(fichier)
        doc = parse(flux)
        flux.close()
        el = doc.childNodes[0]
        self.ouvrir(jeu.charger(el))
        self.register_recent()

    def sauver(self, fichier=None):
        if fichier is None and self.fichier is None:
            d = self.glade.get_widget('select_fichier_enr')
            d.set_current_folder(xdgbd._home)
            d.set_current_name("%s.cifrado"%(self.jeu.nom))
            r = d.run()
            d.hide()
            if r != gtk.RESPONSE_OK.__index__():
                return
            else:
                fichier = self.fichier = d.get_filename()
        else:
            fichier = self.fichier

        doc = Document()
        doc.appendChild(self.jeu.createXML(doc))
        flux = open(urllib.unquote(fichier.replace("file://","")), "w")
        flux.write(doc.toprettyxml())
        flux.close()
        self.register_recent()
        print "Jeu ",self.jeu.nom," sauvé dans ",self.fichier
        self.sync(True)

    def sync(self, sync):
        self.jeu.sync = sync;
        wids = ['enregistrer_btn', 'menu_jeu_enr']
        for wid in wids:
            self.glade.get_widget(wid).set_sensitive(not self.jeu.sync);

    def register_recent(self):
        rec = gtk.recent_manager_get_default()
        if rec.has_item(self.fichier):
            rec.remove_item(self.fichier)

        recent_data = {"mime_type": "application/x-cifrado+xml",
                       "app_name": "Cifrado",
                       "app_exec": "cifrado %u",
                       "display_name": "Grand-jeu « "+self.jeu.nom+" »",
                       "description": "Grand-jeu « "+self.jeu.nom+" »",
                       "is_private": False,
                       "groups" : ['cifrado']}
        rec.add_full(self.fichier, recent_data)
        print self.fichier, " dans les documents récents"


    def nouveau(self):
        self.fermer()
        nj = jeu.Jeu(nom='Nouveau Jeu',
                     trame="Raconter ici votre trame imaginaire",
                     nb_chefs=4)
        self.fichier = None

        nj.ajouter_lieu(Lieu('Nouveau lieu'))
        nj.ajouter_force(Force('Nouvelle force'))
        self.ouvrir(nj)
        self.glade.get_widget('msg_nv_btn').clicked()
        self.sync(True)


    def fermer(self):
        if not self.jeu:
            return
        if not self.jeu.sync:
            d = gtk.MessageDialog(self.glade.get_widget('cifrado_fenetre'),
                                  gtk.DIALOG_MODAL,
                                  gtk.MESSAGE_WARNING,
                                  gtk.BUTTONS_YES_NO,
                                  None)
            d.set_markup("<big><b>Voulez-vous sauvegarder ?</b></big>")
            d.format_secondary_text("Le jeu en édition n'est pas sauvegardé. "
                                    "Si vous le fermer maintenant, toute vos "
                                    "modifications seront perdues !")
            res = d.run()
            d.destroy()
            if res == gtk.RESPONSE_YES.__index__():
                self.sauver()

        self.glade.get_widget('jeu_nom_champ').set_text('')
        self.glade.get_widget('jeu_nb_chefs_champ').set_value(0.)

        # champs
        ids = ['jeu_trame_champ', 'msg_champ']
        for id in ids:
            self.glade.get_widget(id).get_buffer().set_text('')

        # listes
        ids = ['forces_liste', 'lieux_liste', 'msg_liste', 'em_dest_select',
               'em_lieu_select']
        for id in ids:
            self.glade.get_widget(id).set_model(None)

        ids = ['cifrado_onglets', 'imprimer_btn', 'enregistrer_btn',
               'menu_jeu_fermer', 'menu_jeu_enr', 'menu_jeu_enr_sous', 'menu_jeu_imprimer',
               'menu_police_choisir']
        for id in ids:
            self.glade.get_widget(id).set_sensitive(False)

        # fenêtre
        self.glade.get_widget('cifrado_fenetre').set_title('Cifrado')

        self.jeu = None
        self.fichier = None
        

    def ouvrir(self, jeu):
        self.jeu = jeu
        self.glade.get_widget('jeu_nom_champ').set_text(jeu.nom)
        self.glade.get_widget('jeu_trame_champ').get_buffer().set_text(jeu.trame)
        self.glade.get_widget('jeu_nb_chefs_champ').set_value(float(jeu.nb_chefs))
        self.glade.get_widget('forces_liste').set_model(jeu.forces)
        self.glade.get_widget('lieux_liste').set_model(jeu.lieux)
        self.msg_exp_renderer.set_property('model', jeu.forces)
        self.glade.get_widget('msg_liste').set_model(jeu.messages)
        self.glade.get_widget('em_dest_select').set_model(jeu.forces)
        self.glade.get_widget('em_lieu_select').set_model(jeu.lieux)
        self.glade.get_widget('selecteur_police_dialog').set_font_name(jeu.police);

        ids = ['cifrado_onglets', 'imprimer_btn', 'enregistrer_btn',
               'menu_jeu_fermer', 'menu_jeu_enr', 'menu_jeu_enr_sous', 'menu_jeu_imprimer',
               'menu_police_choisir']
        for id in ids:
            self.glade.get_widget(id).set_sensitive(True)
        self.sync(True)


    def get_msg_crt(self):
        liste = self.glade.get_widget('msg_liste')
        sel = liste.get_selection()
        model, iter = sel.get_selected()
        if model is not None and model.iter_is_valid(iter):
            msg = model.get_value(iter, jeu.COL_MSG)
        else:
            msg = None
        return msg

    def get_em_crt(self):
        iter = self.glade.get_widget('em_dest_select').get_active_iter()
        if iter:
            dest = self.jeu.forces.get_value(iter, jeu.COL_FORCE)
            msg = self.get_msg_crt()
            return msg.emissions[dest]
        else:
            return None
        


    #####################
    #   callbacks       #
    #####################

    # JEU

    def on_jeu_nom_champ_changed(self, champ):
        self.jeu.nom = champ.get_text()
        self.glade.get_widget('cifrado_fenetre').set_title(self.jeu.nom)
        self.sync(False)

    def on_jeu_nb_chefs_champ_value_changed(self, bouton):
        self.jeu.nb_chefs= bouton.get_value_as_int()
        self.sync(False)

    def on_trame_changed(self, tampon):
        self.jeu.trame = tampon.get_text(tampon.get_start_iter(),
                                         tampon.get_end_iter())
        self.sync(False)

    def on_champ_exp_activate(self, widget):
        box = widget.get_parent()
        box.set_child_packing(widget, not widget.get_property('expanded'),
                              True, 0, gtk.PACK_START)

    # FORCES
    def on_force_nv_btn_clicked(self, bouton):
        ne = Force('Nouvelle force')
        self.jeu.forces.append([ne.nom, ne])
        self.sync(False)

    def on_force_nom_edited(self, renderer, path, new_text):
        iter = self.jeu.forces.get_iter(path)
        f = self.jeu.forces.get_value(iter, jeu.COL_FORCE)
        f.nom = new_text
        self.jeu.forces.set(iter, jeu.COL_FORCE_NOM, f.nom)
        self.jeu.messages.foreach(self.force_nom_edited_msg_foreach)
        self.sync(False)

    def force_nom_edited_msg_foreach(self, model, path, iter):
        msg = model.get_value(iter, jeu.COL_MSG)
        model.set(iter,
                  jeu.COL_MSG_EXP, msg.exp.nom)

    def on_force_suppr_btn_clicked(self, bouton):
        tree, iter = self.glade.get_widget('forces_liste').get_selection().get_selected()
        if iter is not None:
            self.jeu.forces.remove(iter)
        self.sync(False)
        
    # LIEUX
    def on_lieu_nv_btn_clicked(self, bouton):
        nl = Lieu('Nouveau lieu')
        self.jeu.lieux.append([nl.nom, nl])
        self.sync(False)

    def on_lieu_nom_edited(self, renderer, path, new_text):
        iter = self.jeu.lieux.get_iter(path)
        l = self.jeu.lieux.get_value(iter, jeu.COL_LIEU)
        l.nom = new_text
        self.jeu.lieux.set(iter,
                           jeu.COL_LIEU_NOM, l.nom)
        self.sync(False)

    def on_lieu_suppr_btn_clicked(self, bouton):
        tree, iter = self.glade.get_widget('lieux_liste').get_selection().get_selected()
        if iter is not None:
            lieu = self.jeu.lieux.get_value(iter, jeu.COL_LIEU)
            lieu.nom = None
            self.jeu.lieux.remove(iter)
        self.sync(False)
        

    # MESSAGES
    def on_msg_nv_btn_clicked(self, bouton):
        # choisir un expéditeur
        tree, iter = self.glade.get_widget('forces_liste').get_selection().get_selected()
        if iter is None:
            iter = self.jeu.forces.get_iter_first()
        exp = self.jeu.forces.get_value(iter, jeu.COL_FORCE)

        # créer les émissions
        msg = message.Message(exp=exp,
                              titre="Nouveau message",
                              date="jour/heure",
                              message="",
                              jeu=self.jeu)
        iter = self.jeu.ajouter_message(msg)
        self.glade.get_widget('msg_liste').get_selection().select_iter(iter)
        self.sync(False)

    def on_msg_selected(self, selection):
        sync = self.jeu.sync;
        liste, iter = selection.get_selected()

        wids = ['msg_impr_btn', 'msg_suppr_btn', 'msg_onglets']
        for id in wids:
            self.glade.get_widget(id).set_sensitive(iter is not None)

        if iter:
            msg = liste.get_value (iter, jeu.COL_MSG)
            self.glade.get_widget('msg_champ').get_buffer().set_text(msg.message)
            # sélectionner le premier destinataire
            ems = self.glade.get_widget('em_dest_select')
            model = ems.get_model()
            iter = model.get_iter_first()
            while iter is not None and model.iter_is_valid(iter):
                dest = model.get_value(iter, jeu.COL_FORCE)
                em = self.get_msg_crt().emissions[dest]
                if em.env is True:
                    break
                iter = model.iter_next(iter)

            # ou par défaut le premier
            if iter is None or not model.iter_is_valid(iter):
                iter = model.get_iter_first()

            ems.set_active_iter(iter)
            self.on_em_dest_select_changed(ems)
            
        else:
            self.glade.get_widget('msg_champ').get_buffer().set_text('')
        self.jeu.sync = sync


    def on_msg_exp_edited(self, renderer, path, force):
        iter = self.jeu.messages.get_iter(path)
        self.jeu.messages.set(iter,
                              jeu.COL_MSG_EXP, force)
        msg = self.jeu.messages.get_value(iter, jeu.COL_MSG)
        msg.exp = self.jeu.get_force_depuis_nom (force)
        self.sync(False)

    def on_msg_titre_edited(self, renderer, path, titre):
        iter = self.jeu.messages.get_iter(path)
        msg = self.jeu.messages.get_value(iter, jeu.COL_MSG)
        msg.titre = titre
        self.jeu.messages.set(iter, jeu.COL_MSG_TITRE, titre)
        self.sync(False)
        
    def on_msg_date_edited(self, renderer, path, date):
        iter = self.jeu.messages.get_iter(path)
        msg = self.jeu.messages.get_value(iter, jeu.COL_MSG)
        msg.date = date
        self.jeu.messages.set(iter, jeu.COL_MSG_DATE, date)
        self.sync(False)
        
    def on_msg_suppr_btn_clicked(self, bouton):
        liste, iter = self.glade.get_widget('msg_liste').get_selection().get_selected()
        if iter is not None:
            self.jeu.messages.remove(iter)
        self.sync(False)

    def on_msg_changed(self, tampon):
        msg = self.get_msg_crt()
        msg.message = tampon.get_text(tampon.get_start_iter(),
                                      tampon.get_end_iter())

        self.actualiser_substitutions()

        self.sync(False)

    # ÉMISSIONS
    def select_combo(self, combo, col, val):
        model = combo.get_model()
        i = model.get_iter_first()
        while i is not None:
            cell = model.get_value(i, col)
            if cell == val:
                combo.set_active_iter(i)
                break
            i = model.iter_next(i)

    def on_em_dest_select_changed(self, combo):
        sync = self.jeu.sync
        em = self.get_em_crt()
        wids = ['em_env_champ']
        envc = self.glade.get_widget('em_env_champ')
        if em:
            for wid in wids:
                self.glade.get_widget(wid).set_sensitive(True)
            envc.set_active(em.env)
            self.select_combo(self.glade.get_widget('em_lieu_select'), jeu.COL_LIEU, em.lieu)
            self.glade.get_widget('codeur_liste').set_model(em.codeurs)
        else:
            for wid in wids:
                self.glade.get_widget(wid).set_sensitive(False)
            self.glade.get_widget('codeur_liste').set_model(None)
            envc.set_active(False)

        self.actualiser_substitutions()

        envc.toggled()
        self.jeu.sync = sync

    def on_em_env_champ_toggled(self, bouton):
        em = self.get_em_crt()
        em.env = bouton.get_active()
        wids = ['em_lieu_select', 'chiffrement_exp', 'subs_exp',
                'codeur_nv_btn', 'codeur_prop_btn', 'codeur_conv_btn',
                'codeur_suppr_btn']
        for wid in wids:
            self.glade.get_widget(wid).set_sensitive(em.env)
        self.sync(False)

    def on_em_lieu_select_changed(self, combo):
        iter = combo.get_active_iter()
        lieu = combo.get_model().get_value(iter, jeu.COL_LIEU)
        self.get_em_crt().lieu = lieu
        self.sync(False)

    # CODEUR
    def get_cod_crt(self):
        model, iter = self.glade.get_widget('codeur_liste').get_selection().get_selected()
        return model.get_value(iter, emission.COL_COD)
        

    def on_codeur_nv_btn_clicked(self, bouton):
        em = self.get_em_crt()
        iter = em.ajouter_codeur(codeurs.aucun.Codeur())
        self.glade.get_widget('codeur_liste').get_selection().select_iter(iter)
        self.sync(False)

    def on_codeur_selection_changed(self, selection):
        model, iter = selection.get_selected()
        wids = ['codeur_prop_btn', 'codeur_suppr_btn']
        sensitive = iter is not None
        for wid in wids:
            self.glade.get_widget(wid).set_sensitive(sensitive)

        if sensitive:
            self.setup_codeur_prop()

    def on_codeur_prop_btn_clicked(self, bouton):
        d = self.glade.get_widget('codeur_prop_dialog')
        d.show_all()
        d.run()
        d.hide()
        cod = self.get_cod_crt()
        model, iter = self.glade.get_widget('codeur_liste').get_selection().get_selected()
        model.set(iter, emission.COL_COD_PROP, cod.get_prop_desc())
        if self.get_cod_crt().__class__ is codeurs.police.Codeur:
            model.set(iter, emission.COL_COD_POL, cod.get_police())
        self.sync(False)

    def on_codeur_conv_btn_clicked(self, bouton):
        b = self.glade.get_widget('conversion_champ').get_buffer()
        b.set_text(self.get_em_crt().convertir(self.get_msg_crt().message))
        d = self.glade.get_widget('conversion_dialog')
        r = d.run()
        d.hide()
        if r == gtk.RESPONSE_APPLY.__index__():
            c = gtk.clipboard_get(gtk.gdk.SELECTION_CLIPBOARD)
            b.select_range(b.get_start_iter(), b.get_end_iter())
            b.copy_clipboard(c)

    def on_codeur_suppr_btn_clicked(self, bouton):
        model, iter = self.glade.get_widget('codeur_liste').get_selection().get_selected()
        model.remove(iter)
        self.sync(False)

    def get_cod_classe_depuis_nom(self, nom):
        iter = self.codeurs.get_iter_first()
        while iter is not None:
            tn, classe = self.codeurs.get(iter, COL_COD_NOM, COL_COD_CLASSE)
            if tn == nom:
                return classe
            iter = self.codeurs.iter_next(iter)

    def on_codeur_edited(self, renderer, path, texte):
        classe = self.get_cod_classe_depuis_nom(texte)
        iter = self.glade.get_widget('codeur_liste').get_model().get_iter(path)
        em = self.get_em_crt()
        em.changer_codeur(iter, classe)
        self.setup_codeur_prop()
        self.sync(False)

    def setup_codeur_prop(self):
        cod = self.get_cod_crt()

        w = self.glade.get_widget('codeur_prop_cont').get_child()
        if w:
            w.destroy()

        w = cod.get_prop_widget()

        if w is not None:
            self.glade.get_widget('codeur_prop_cont').add(w)

        self.glade.get_widget('codeur_prop_btn').set_sensitive(w is not None)


    # SUBSTITUTIONS
    def actualiser_substitutions(self):
        em = self.get_em_crt()
        if em is None:
            return
        
        # substitutions
        msg = self.get_msg_crt()
        self.substitutions.clear()
        variables = msg.lister_variables()
        utile = len(variables) != 0
        exp = self.glade.get_widget('subs_exp')
        exp.set_expanded(utile)
        exp.set_sensitive(utile)

        if utile is True:
            for var in variables:
                self.substitutions.append([var, em.get_val(var)])
        

    def on_substitution_edited(self, renderer, path, valeur):
        iter = self.substitutions.get_iter(path)
        var = self.substitutions.get_value(iter, emission.COL_SUB_VAR)
        self.get_em_crt().set_val(var, valeur)
        self.substitutions.set_value(iter, emission.COL_SUB_VAL, valeur)
        self.sync(False)


    # NOUVEAU
    def on_menu_jeu_nv_activate(self, item):
        self.nouveau()

    # CHARGER
    def on_menu_jeu_ouv_activate(self, item):
        self.charger(None)

    # SAUVER
    def on_enregistrer_btn_clicked(self, bouton):
        self.sauver(self.fichier)

    def on_menu_jeu_enr_activate(self, item):
        self.sauver(self.fichier)

    def on_menu_jeu_enr_sous_activate(self, item):
        self.sauver(None)





    # IMPRIMER
    def on_menu_jeu_page_prop_activate(self, item):
        gtk.print_run_page_setup_dialog(self.glade.get_widget('cifrado_fenetre'),
                                        self.page_setup,
                                        self.print_settings)

    def on_menu_jeu_imprimer_activate(self, item):
        self.imprimer(None)

    def on_imprimer_btn_clicked(self, bouton):
        self.imprimer(None)

    def on_msg_impr_btn_clicked(self, bouton):
        self.imprimer(self.get_msg_crt())


    def impr_begin_print(self, op, contexte):
        pass

    def impr_draw_page(self, op, contexte, no):
        if self.pages[no] is self.jeu:
            self.impr_page_chef(op, contexte, no)
        else:
            self.impr_message(op, contexte, no)


    def impr_message(self, op, contexte, no):
        n, msg, em = self.pages[no]

        message = msg
        equipe = dest = em.dest
        dc = em.get_dernier_codeur()

        police = dc.get_police()
        if police is None:
            police = self.jeu.police

        ih = 48
        marge = 0
        cr = contexte.get_cairo_context()
        pg = contexte.create_pango_context()
        division = 9

        fd = pango.FontDescription('sans normal 9')

        # Titres : on affiche nom de l'équipe, nom du message et
        # identifiant pour la maîtrise.
        titre = "Message n°%i : « %s » de « %s » à « %s » ; Déposer à %s ; %s"%(n,message.titre,message.exp.nom,dest.nom,
                                                                                em.lieu.nom,msg.date)
        w, hh = self.impr_text(contexte, titre, font='sans normal 9')

        # on affiche uniquement l'identifiant pour les scouts
        id = "%i"%(n)
        layout = self.layout_text(contexte, id,
                                  font='sans normal 7',
                                  align=pango.ALIGN_RIGHT)
        w, h = layout.get_size()
        self.impr_layout(contexte, layout,
                         y=contexte.get_height() - 2*h/pango.SCALE)

        # séparateur
        cr.new_path()
        cr.save()
        cr.set_source_rgb(.5, .5, .5)
        cr.move_to(0,
                   contexte.get_height()/division
                   +self.page_setup.get_bottom_margin(gtk.UNIT_POINTS))
        cr.rel_line_to(contexte.get_width(), 0)
        cr.set_line_width(.5)
        cr.set_dash([2.,8.])
        cr.stroke()
        cr.restore()

        # BANDEAU CHEF
        cr.rectangle(0, 0,
                     contexte.get_width(),
                     contexte.get_height()/division
                     +self.page_setup.get_bottom_margin(gtk.UNIT_POINTS)-2)
        cr.clip()

        # chiffrements
        hw,h = self.impr_text(contexte, "Chiffré avec",
                              font='sans bold 9',
                              y=hh/pango.SCALE,
                              align=pango.ALIGN_RIGHT)

        iter = em.codeurs.get_iter_first()
        mw = 0
        layouts = []
        while iter is not None and em.codeurs.iter_is_valid(iter):
            codeur = em.get_codeur(iter)

            texte = em.get_codeur_nom(iter)
            prop = codeur.get_prop_desc()
            if prop is not None:
                texte+=' ( '+prop+' )'

            layout = self.layout_text(contexte, texte,
                                 font='sans normal 7')
            w,h = layout.get_size()
            mw = max(mw, w)

            layouts.append(layout)
            
            iter = em.codeurs.iter_next(iter)

        il = 3                  # interligne
        wc = 2*il+max(hw,mw)/pango.SCALE    # largeur de colonne
        mw/= pango.SCALE        # largeur max
        y = hh + il*pango.SCALE + h + il*pango.SCALE

        for layout in layouts:
            self.impr_layout(contexte, layout,
                             x=contexte.get_width()-mw,
                             y=y/pango.SCALE)
            w,h = layout.get_size()
            y+=h+il*pango.SCALE

        # séparateur vertical
        cr.new_path()
        cr.save()
        cr.set_source_rgb(.5, .5, .5)

        cr.move_to(contexte.get_width()-wc-il,
                   hh/pango.SCALE)
        cr.rel_line_to(0, (y-hh)/pango.SCALE)
        cr.set_line_width(.5)
        cr.stroke()
        cr.restore()

        texte = em.substituer(message.message,
                              message.lister_variables())
        
        # Original
        self.impr_text(contexte, texte,
                       font='sans normal 7',
                       y=int(1.5 * hh/pango.SCALE),
                       width=contexte.get_width()*pango.SCALE-(il+wc)*pango.SCALE,
                       justify=True)

        cr.reset_clip()         # fin du bandeau chef

        # Chiffré
        layout = self.layout_text(contexte, em.convertir(texte),
                                  font=police,
                                  justify=True)
        w,h = layout.get_size()
        layout.set_spacing(int(dc.get_interligne()* h / layout.get_line_count()))
        self.impr_layout(contexte, layout,
                         y=contexte.get_height()/division
                         +self.page_setup.get_bottom_margin(gtk.UNIT_POINTS)
                         +self.page_setup.get_top_margin(gtk.UNIT_POINTS))


    def layout_text(self, contexte, text,
                    width=None,
                    align=pango.ALIGN_LEFT,
                    font='sans normal 9',
                    justify=False):
        """return layout"""
        layout = contexte.create_pango_layout()
        if width is None:
            width = contexte.get_width() * pango.SCALE
        layout.set_width(int(width))
        layout.set_wrap(pango.WRAP_WORD)
        layout.set_alignment(align);
        fd = pango.FontDescription(font)
        layout.set_font_description(fd)
        layout.set_text(text)
        return layout

    def impr_layout(self, contexte, layout,x=0,y=0):
        cr = contexte.get_cairo_context()
        cr.move_to(x, y)
        cr.layout_path(layout)
        cr.fill()
        return layout.get_size()

    def impr_text(self, contexte, text, x=0, y=0,
                  width=None,
                  align=pango.ALIGN_LEFT,
                  font='sans normal 9',
                  justify=False):
        """return layout size in pango unit"""
        if width is None:
            width = (contexte.get_width()-x) * pango.SCALE
        layout = self.layout_text(contexte, text, width, align, font, justify)
        return self.impr_layout(contexte, layout,x,y)

    def impr_page_chef(self, op, contexte, no):
        cr = contexte.get_cairo_context()
        pg = contexte.create_pango_context()
        # interligne en point cairo
        il = 8

        # TITRE
        w,h = self.impr_text(contexte, self.jeu.nom,
                       font='sans bold 16',
                       align=pango.ALIGN_CENTER)
        th = h/pango.SCALE+il+il

        # FORCES EN PRÉSENCE
        w,h = self.impr_text(contexte, "Forces en présence",y=th,
                             font='sans bold 12')
        sth = th
        sth+=h/pango.SCALE+il

        text = ""
        iter = self.jeu.forces.get_iter_first()
        while iter is not None and self.jeu.forces.iter_is_valid(iter):
            force = self.jeu.forces.get_value(iter, jeu.COL_FORCE)
            text+= force.nom+"\n"
            iter = self.jeu.forces.iter_next(iter)
        
        w,h = self.impr_text(contexte,text,x=il,y=sth)
        sth+=h/pango.SCALE+il

        # LIEU
        lw = contexte.get_width()/2
        w,h = self.impr_text(contexte, "Lieux",x=lw,y=th,
                             font='sans bold 12')
        th+=h/pango.SCALE+il

        text = ""
        iter = self.jeu.lieux.get_iter_first()
        while iter is not None and self.jeu.lieux.iter_is_valid(iter):
            lieu = self.jeu.lieux.get_value(iter, jeu.COL_LIEU)
            text+= lieu.nom+"\n"
            iter = self.jeu.lieux.iter_next(iter)
        
        w,h = self.impr_text(contexte,text,x=lw+il,y=th)
        th+=h/pango.SCALE+il

        th = max(th,sth)

        # TRAME
        w,h = self.impr_text(contexte, "Trame imaginaire",y=th,
                             font='sans bold 12')
        th+=h/pango.SCALE+il
        self.impr_text(contexte, self.jeu.trame,x=il,y=th,
                       justify=True)


    # FERMER
    def on_menu_jeu_fermer_activate(self, item):
        self.fermer()

    # QUITTER
    def on_cifrado_fenetre_delete_event(self, fenetre, event):
        gobject.idle_add(self.quitter)

    def on_menu_jeu_quitter_activate(self, item):
        gobject.idle_add(self.quitter)

    def on_quitter_btn_clicked_event(self, bouton):
        gobject.idle_add(self.quitter)

    # À PROPOS
    def on_menu_aide_apropos_activate(self, item):
        apropos = self.glade.get_widget('apropos')
        apropos.run()
    
    def on_apropos_response(self, apropos, reponse):
        apropos.hide()

    def on_menu_aide_aide_activate(self, item):
        gnome.help_display_uri('ghelp:cifrado')

    # POLICE
    def on_menu_police_choisir_activate(self, item):
        dialog = self.glade.get_widget('selecteur_police_dialog')
        dialog.run()

    def on_selecteur_police_dialog_response(self, dialog, response):
        if response == gtk.RESPONSE_OK:
            police = dialog.get_font_name()
            self.sync(police == self.jeu.police)
            self.jeu.police = police
        dialog.hide()
