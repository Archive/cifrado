__all__ = ['aucun', 'braille', 'caesar', 'chiffrer', 'inverser', 'morpion', 'morse',
           'police', 'roulement', 'samourai', 'templier', 'vigenere']
