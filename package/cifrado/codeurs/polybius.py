# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

import  gtk
from    ..      import    codeur

def charger(node):
    codeur = Codeur()
    return codeur

table = [['A', 'B', 'C', 'D', 'E'],
         ['F', 'G', 'H', 'I', 'J'],
         ['K', 'L', 'M', 'N', 'O'],
         ['P', 'Q', 'R', 'S', 'T'],
         ['U', 'V', 'X', 'Y', 'Z']]

class Codeur(codeur.Codeur):
    """Implémente le codage de Polybius"""
    nom = 'Polybius'

    def convertir(self, texte):
        """Converti le texte en remplaçant une lettre par ses coordonnées dans
la table. Attention, pas de parenthèses ni virgules"""
        texte = self.strip_special_chars(texte)
        result = ''

        a = False
        for char in texte:
            if char.isalpha():
                # on n'a que 25 cases …
                if char == 'W':
                    char = 'V'
                if a:
                    result+='/'
                for i in range(5):
                    for j in range(5):
                        if char.upper() == table[i][j]:
                            result+=str(i)+str(j)
            else:
                result+=char
            a = char.isalpha()

        return result
