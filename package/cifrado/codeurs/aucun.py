# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

from    ..      import    codeur

def     charger(node):
    return Codeur()

class Codeur(codeur.Codeur):
    nom = 'Aucun'

    def get_interligne(self):
        return 0
