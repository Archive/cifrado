# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

import  gtk
from    ..      import    codeur

def charger(node):
    codeur = Codeur()
    return codeur
    
class Codeur(codeur.Codeur):
    nom = 'Inverser'

    def convertir(self, text):
        """Inverse l'ordre de l'alphabêt"""
        text = self.strip_special_chars(text)
        result = ''

        for char in text:
            if (char.isalpha()):
                casse = self.casse(char)
                index = self.letters[casse].index(char)
                result+= self.letters[casse][25-index]
            else:
                result+= char

        return result
