# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

import  gtk
from    ..      import    codeur

def charger(node):
    codeur = Codeur(originale=node.getAttribute('originale'),
                    finale=node.getAttribute('finale'))
    return codeur
    
class Codeur(codeur.Codeur):
    nom = "Roulement"

    def __init__(self, originale='A', finale='K'):
        self.originale = originale
        self.finale = finale
        self.decalage = self.letters['upper'].index(finale) - self.letters['upper'].index(originale)
        self.original_selector = None
    
    def convertir(self, text):
        """Décale l'aphabêt suivant le choix de l'utilisateur."""
        text = self.strip_special_chars(text)
        result = "";
        
        for char in text:
            if (char.isalpha()):
                if (char.islower()):
                    liste = self.letters["lower"]
                else:
                    liste = self.letters["upper"]
                    
                result+=liste[(liste.index(char) - self.decalage) % 26]
            else:
                result+= char
            
        return result

    def get_prop_widget(self):
        box = gtk.HBox()
        box.set_spacing(6)

        self.original_selector = gtk.combo_box_new_text()
        self.original_selector.connect("changed", self.on_combo_changed, box)
        model = self.original_selector.get_model()
        for letter in self.letters["upper"]:
            model.append([letter])

        box.pack_start(self.original_selector, False, True)
        
        self.etiq = gtk.Label('devient')
        box.pack_start(self.etiq, False, True)

        self.final_selector = gtk.combo_box_new_text()
        self.final_selector.connect("changed", self.on_combo_changed, box)
        model = self.final_selector.get_model()
        for letter in self.letters["upper"]:
            model.append([letter])
        
        box.pack_start(self.final_selector, False, True)

        box.show_all()
        self.original_selector.set_active(0) # A
        self.final_selector.set_active(self.letters["upper"].index("K")) # vaut K
       
        return box

    def get_prop_desc(self):
        return self.originale+self.get_etiq()+self.finale

    def get_etiq(self):
        """Détermine l'étiquette à intercaller entre les lettres"""

        etiqs = {'AK': ' vaut ',
                 'LN': '',
                 'HS': ''}      # d'autres ?

        etiq = ' devient '
        for clef in etiqs.keys():
            if self.originale == clef[0] and self.finale == clef[1]:
                etiq = etiqs[clef]
                break

        return etiq
            
    def sauver(self, doc, element):
        element.setAttribute('originale', self.originale)
        element.setAttribute('finale', self.finale)

    # CALLBACK
    def on_combo_changed(self, widget, box):
        self.finale = self.letters['upper'][self.final_selector.get_active()]
        self.originale = self.letters['upper'][self.original_selector.get_active()]
        self.decalage = self.final_selector.get_active() - self.original_selector.get_active()
        self.changed()

