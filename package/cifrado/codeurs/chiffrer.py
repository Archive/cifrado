# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

import  gtk
from    ..      import    codeur

def charger(node):
    codeur = Codeur(lettre=node.getAttribute('lettre'),
                    nombre=int(node.getAttribute('nombre')))
    return codeur
    
class Codeur(codeur.Codeur):
    nom = 'Chiffrer'

    def __init__(self, lettre='D', nombre=3):
        self.lettre = lettre
        self.nombre = nombre
        self.decalage = nombre - self.letters['upper'].index(lettre)
        self.letter_selector = None
        
    def convertir(self, text):
        """Converti les  letres en chiffre suivant un décalage paramétré."""
        text = self.strip_special_chars(text)
        result = ''

        for char in text:
            if (char.isalpha()):
                casse = self.casse(char)
                index = (self.letters[casse].index(char) + self.decalage) % 26
                result+= str(index+1)+' '
            else:
                if (char.isspace()):
                    result+= char+' '
                else:
                    result+= char

        return result
    
    def get_prop_widget(self):
        box = gtk.HBox()
        box.set_spacing(6)

        self.letter_selector = gtk.combo_box_new_text()
        self.letter_selector.connect('changed', self.on_combo_changed, box)
        model = self.letter_selector.get_model()
        for letter in self.letters['upper']:
            model.append([letter])
        box.pack_start(self.letter_selector, False, True)
        
        self.number_selector = gtk.combo_box_new_text()
        self.number_selector.connect('changed', self.on_combo_changed, box)
        model = self.number_selector.get_model()
        for number in range(1, 26):
            model.append([number])
        
        box.pack_start(self.number_selector, False, True)
        box.show_all()
        
        self.letter_selector.set_active(self.letters['upper'].index(self.lettre))
        self.number_selector.set_active(self.nombre-1)

        return box

    def get_prop_desc(self):
        return self.lettre+str(self.nombre)

    def sauver(self, doc, element):
        element.setAttribute('lettre', self.lettre)
        element.setAttribute('nombre', str(self.nombre))

    # CALLBACK
    def on_combo_changed(self, widget, box):
        self.lettre = self.letters['upper'][self.letter_selector.get_active()]
        self.nombre = self.number_selector.get_active()+1
        self.decalage = self.number_selector.get_active() - self.letter_selector.get_active()
        self.changed()
