# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

import  gtk

from    ..      import    codeur

def     charger(node):
    return Codeur(node.hasAttribute('inverser'))

class Codeur(codeur.Codeur):
    nom = 'Morse'

    def __init__(self, inverser=False):
        self.inverser = inverser
        self.check = None

    def get_prop_widget(self):
        self.check = gtk.CheckButton("Inverser · et —")
        self.check.set_active(self.inverser)
        self.check.connect("toggled", self.check_toggled)
        return self.check

    def get_prop_desc(self):
        if self.inverser:
            return 'inversion'
        else:
            return None

    def get_police(self):
        return 'sans normal 9'

    def get_interligne(self):
        return 1.25
    
    def convertir(self, text):
        text = self.strip_special_chars(text)
        
        table = {'a' : '.-',
                 'b' : '-...',
                 'c' : '-.-.',
                 'd' : '-..',
                 'e' : '.',
                 'f' : '..-.',
                 'g' : '--.',
                 'h' : '....',
                 'i' : '..',
                 'j' : '.---',
                 'k' : '-.-',
                 'l' : '.-..',
                 'm' : '--',
                 'n' : '-.',
                 'o' : '---',
                 'p' : '.--.',
                 'q' : '--.-',
                 'r' : '.-.',
                 's' : '...',
                 't' : '-',
                 'u' : '..-',
                 'v' : '...-',
                 'w' : '.--',
                 'x' : '-..-',
                 'y' : '-.--',
                 'z' : '--..',
                 
                 '0' : '-----',
                 '1' : '.----',
                 '2' : '..---',
                 '3' : '...--',
                 '4' : '....-',
                 '5' : '.....',
                 '6' : '-....',
                 '7' : '--...',
                 '8' : '---..',
                 '9' : '----.',

                 '.' : '.-.-.-',
                 ',' : '--..--',
                 '?' : '..--..',
                 "'" : '.----.',
                 '!' : '-.-.--',
                 '/' : '-..-.',
                 '(' : '-.--.-',
                 ')' : '-.--.-',
                 '&' : '. ...',
                 ':' : '---...',
                 ';' : '-.-.-.',
                 '=' : '-...-',
                 '-' : '-....-',
                 '_' : '..-- .-',
                 '\'': '.-..-.',
                 '$' : '...-..-',
                 '@' : '.--.-',

                 'æ' : '.-.-',
                 'à' : '.--.-',
                 'ç' : '-.-..',
                 'è' : '.-..-',
                 'é' : '..-..',

                 ' ' : ''
                 }

        text = text.lower()
        result = ''

        for char in text:
            try:
                for m in table[char]:
                    result+= m+' '
                result+='/ '
            except:
                result+= char

        if not self.inverser:
            result = result.replace ('.', '·')
            result = result.replace ('-', '—')
        else:
            result = result.replace ('.', '—')
            result = result.replace ('-', '·')

        return result

    def sauver(self, doc, el):
        if self.inverser:
            el.addAttribute('inverser', 'inverser')

    def check_toggled(self, bouton):
        self.inverser = bouton.get_active()
