# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

import  gtk
from    ..      import    codeur

def     charger(node):
    return Codeur(node.getAttribute('police'))

class Codeur(codeur.Codeur):
    nom = 'Police'

    def __init__(self, police = 'sans normal 9'):
        self.police = police
        win = gtk.Window()
        sel = gtk.FontSelection()
        win.add(sel)
        sel.set_font_name(self.police)
        win.remove(sel)
        self.sel = sel

    def get_interligne(self):
        # attention, ça casse les précédents chiffrage s'ils ont un
        # interligne !
        return 0

    def get_police(self):
        self.police = self.sel.get_font_name()
        return self.police

    def sauver(self, doc, el):
        el.setAttribute('police', self.get_police())
    
    def get_prop_widget(self):
        return self.sel

    def get_prop_desc(self):
        return self.get_police()

