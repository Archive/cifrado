# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

from    ..      import    codeur

def     charger(node):
    return Codeur()

class Codeur(codeur.Codeur):
    nom = 'Braille'

    def convertir(self, text):
        return self.strip_special_chars(text)

    def get_police(self):
        return 'Braille 16'

