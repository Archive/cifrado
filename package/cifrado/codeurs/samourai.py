# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

import  gtk
from    ..      import    codeur

def     charger(node):
    return Codeur()

class Codeur(codeur.Codeur):
    nom = 'Samouraï'

    def convertir(self, text):
        return self.strip_special_chars(text)

    def get_police(self):
        return 'Samourai 14'

