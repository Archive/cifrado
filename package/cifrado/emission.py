# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

import gobject
import pango
import gtk

from    .       import codeur
from    .       import codeurs

COL_COD_NOM, COL_COD_PROP, COL_COD_POL, COL_COD = range(4)
COL_SUB_VAR, COL_SUB_VAL = range(2)

def charger(el, jeu):
    dest = jeu.get_force_depuis_nom(el.getAttribute('dest'))
    lieu = jeu.get_lieu_depuis_nom(el.getAttribute('lieu'))
    env = el.hasAttribute('envoyer')

    codeurs = []
    for e in el.getElementsByTagName('codeur'):
         codeurs.append(codeur.charger(e))

    substitutions = {}
    for e in el.getElementsByTagName('substitution'):
        substitutions[e.getAttribute('variable')] = e.getAttribute('valeur')

    return Emission(env, dest, lieu, codeurs, substitutions)


class Emission:
    def __init__(self, env, dest, lieu, cods=None, subs=None):
        self.env = env
        self.dest = dest
        self.lieu = lieu
        if subs is None:
            subs = {}
        self.subs = subs
        self.codeurs = gtk.ListStore(gobject.TYPE_STRING,
                                     gobject.TYPE_STRING,
                                     gobject.TYPE_STRING,
                                     gobject.TYPE_PYOBJECT)

        if cods is None:
            self.ajouter_codeur(codeurs.aucun.Codeur())
        else:
            for cod in cods:
                self.ajouter_codeur(cod)

                            

    def ajouter_codeur(self, codeur):
        police = 'Sans 10'
        if codeur.__class__ is codeurs.police.Codeur:
            police = codeur.get_police()

        return self.codeurs.append([codeur.nom,
                                    codeur.get_prop_desc(),
                                    police,
                                    codeur])

    def changer_codeur(self, iter, classe):
        codeur = classe()
        police = 'Sans 10'
        if classe is codeurs.police.Codeur:
            police = codeur.get_police()

        self.codeurs.set(iter,
                         COL_COD, codeur,
                         COL_COD_PROP, codeur.get_prop_desc(),
                         COL_COD_POL, police,
                         COL_COD_NOM, classe.nom)

    def get_codeur(self, iter):
        return self.codeurs.get_value(iter, COL_COD)

    def get_codeur_classe(self, iter):
        return self.codeurs.get_value(iter, COL_COD_CLASSE)

    def get_codeur_nom(self, iter):
        return self.codeurs.get_value(iter, COL_COD_NOM)

    def get_val(self, var):
        if not self.subs.has_key(var):
            self.set_val(var, "")
        return self.subs[var]

    def set_val(self, var, val):
        self.subs[var] = val

    def substituer(self, texte, variables):
        for var in variables:
            texte = texte.replace(var, self.get_val(var))
        return texte

    def convertir(self, texte):
        iter = self.codeurs.get_iter_first()
        while iter is not None and self.codeurs.iter_is_valid(iter):
            codeur = self.codeurs.get_value(iter, COL_COD)
            texte = codeur.convertir(texte)
            iter = self.codeurs.iter_next(iter)

        return texte

    def get_dernier_codeur(self):
        iter = self.codeurs.get_iter_first()
        while iter is not None and self.codeurs.iter_is_valid(iter):
            i = iter
            iter = self.codeurs.iter_next(iter)
        return self.codeurs.get_value(i, COL_COD)

    def get_police(self):
        return pango.FontDescription(self.get_dernier_codeur().get_police())

    def get_interligne(self):
        return self.get_dernier_codeur().get_interligne()

    def createXML(self, doc):
        e = doc.createElement('emission')
        if self.env:
            e.setAttribute('envoyer', 'envoyer')
        e.setAttribute('lieu', self.lieu.nom)
        e.setAttribute('dest', self.dest.nom)

        iter = self.codeurs.get_iter_first()
        while iter is not None and self.codeurs.iter_is_valid(iter):
            codeur = self.codeurs.get_value(iter, COL_COD)
            e.appendChild(codeur.createXML(doc))
            iter = self.codeurs.iter_next(iter)

        for var in self.subs:
            if not self.get_val(var) == "":
                s = doc.createElement('substitution')
                s.setAttribute('variable', var)
                s.setAttribute('valeur', self.get_val(var))
                e.appendChild(s)

        return e
