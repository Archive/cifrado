# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

import  gobject
import  gtk
import  jeu     as JEU
import  emission
import  codeurs
import  re

def charger(el, jeu):
    titre = el.getAttribute('titre')
    date = el.getAttribute('date')
    exp = jeu.get_force_depuis_nom(el.getAttribute('exp'))
    message = el.getElementsByTagName('texte')[0].childNodes[0].nodeValue.strip()

    emissions = {}
    for e in el.getElementsByTagName('emission'):
        if jeu.get_force_depuis_nom(e.getAttribute('dest')) is not None:
            em = emission.charger(e, jeu)
            emissions[em.dest] = em

    msg = Message(exp, titre, date, message, jeu, emissions)

    return msg

class Message:
    def __init__(self, exp, titre, date, message, jeu, emissions=None):
        self.exp        = exp
        self.titre      = titre
        self.date       = date
        self.message    = message
        self.capture_vars = re.compile('(\$[A-Z_]{2,})')

        if emissions is None:
            self.emissions = {}
            l = jeu.lieux.get_value(jeu.lieux.get_iter_first(), JEU.COL_LIEU)
            i = jeu.forces.get_iter_first()
            while i is not None and jeu.forces.iter_is_valid(i):
                f = jeu.forces.get_value(i, JEU.COL_FORCE)
                self.emissions[f] = emission.Emission(False, f, l)
                i = jeu.forces.iter_next(i)
        else:
            self.emissions = emissions
            
        jeu.forces.connect('row-changed', self.forces_row_changed, jeu)
        jeu.forces.connect('row-deleted', self.forces_row_deleted)


    def lister_variables(self):
        variables = self.capture_vars.findall(self.message)
        # http://www.peterbe.com/plog/uniqifiers-benchmark
        seen = set()
        variables = [x for x in variables if x not in seen and not seen.add(x)]

        return variables

    def forces_row_changed(self, forces, path, iter, jeu):
        force = forces.get_value(iter, JEU.COL_FORCE)
        lieu = jeu.lieux.get_value(jeu.lieux.get_iter_first(), JEU.COL_LIEU)
        if force is not None and not self.emissions.has_key(force):
            self.emissions[force] = emission.Emission(False, force, lieu, [codeurs.aucun.Codeur()])

    def forces_row_deleted(self, forces, path):
        """On boucle les forces restantes pour les garder. On supprime
        'les' autres."""
        iter = forces.get_iter_first()
        while iter is not None and forces.iter_is_valid(iter): 
            force = forces.get_value(iter, JEU.COL_FORCE)
            garder = False

            for f in self.emissions:
                garder = garder or (f is force)

            if not garder:
                del self.emissions[force]

            iter = forces.iter_next(iter)

    def createXML(self, doc):
        m = doc.createElement('message')
        m.setAttribute('titre', self.titre)
        m.setAttribute('exp', self.exp.nom)
        m.setAttribute('date', self.date)
        
        e = doc.createElement('texte')
        m.appendChild(e)
        e.appendChild(doc.createTextNode(self.message))

        for f in self.emissions:
            m.appendChild(self.emissions[f].createXML(doc))

        return m
