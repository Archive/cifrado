# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

def charger(el):
    return Lieu(el.getAttribute('nom'))

class Lieu:
    def __init__(self, nom=""):
        self.nom        = nom

    def createXML(self, doc):
        l = doc.createElement('lieu')
        l.setAttribute('nom', self.nom)
        return l
