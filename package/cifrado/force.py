# -*- coding: utf-8 -*-

# Cifrado - Chiffrer et imprimer les messages de grand-jeu
# Copyright © 2006-2007 Étienne Bersac — Tous droits réservés.

import  gobject
import  gtk

def charger(el):
    return Force(el.getAttribute('nom'))

class Force:
    def __init__(self, nom=""):
        self.nom        = nom

    def createXML(self, doc):
        f = doc.createElement('force')
        f.setAttribute('nom', self.nom)
        return f
